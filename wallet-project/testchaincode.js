const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

function prettyJSONString(inputString) {
    return JSON.stringify(JSON.parse(inputString), null, 2);
}

async function main() {
    const ccpPath = path.resolve(__dirname, 'connection.json');
    // console.log("ccpPath", ccpPath);
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    const walletPath = path.join(process.cwd(), 'wallet');
    console.log("walletPath", walletPath)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    console.log("wallet", wallet)

    const identity = await wallet.get('Admin@bhupurchasingweapon.com');
    if (!identity) {
        console.log(`An identity for the user "Admin@bhupurchasingweapon.com" does not exist in the wallet`);
        return;
    }

    const gateway = new Gateway();
    try {
        // console.log("wallet",wallet)
        // console.log("identity",identity)

        await gateway.connect(ccp, {
            wallet, identity: identity, discovery: { enabled: true, asLocalhost: false }
        });

        console.log('Connected to the gateway.');

        const network = await gateway.getNetwork('purchaseweaponchannel');
        console.log("network", network)
        const contract = network.getContract('purchasingweaponmgt');
        console.log("contract", contract)


        console.log('\n--> Submit Transaction: Create, function creates');
        await contract.submitTransaction('CreateWeapon', 'pistol', "short pistol");
        console.log('*** Result: committed');




        console.log('\n--> Submit Transaction: ReadWeapon');
        // let result = await contract.submitTransaction('ReadWeapon', '1');
        let result = await contract.evaluateTransaction('ReadWeaponPurchase', '1');

        console.log(`*** Result: ${prettyJSONString(result.toString())}`);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
    } finally {
        gateway.disconnect();
    }
}

main().catch(console.error);