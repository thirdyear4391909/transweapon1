const express = require('express');
const cors = require("cors")
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());
app.use(cors())

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

// Endpoint to create a weapons
app.post('/weapons', async (req, res) => {
    try {
        console.log("postalsikdjla;sk")
        const { name, description, userId } = req.body;
        const result = await submitTransaction(userId, 'CreateWeapon', name, description);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});


// Endpoint to read a weapon by ID
app.get('/weapons/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { userId, org } = req.body;
        // console.log("id", id)
        const result = await evaluateTransaction(userId, org, 'ReadWeapon', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});
// Endpoint to get all weapons
app.get('/weaponsall/:userId', async (req, res) => {
    try {
        const { userId } = req.params;

        const contract = await getContract(userId);

        // Get the total number of weapons
        const totalWeapons = await contract.evaluateTransaction('GetTotalWeapons');
        const numWeapons = parseInt(totalWeapons.toString());

        // Array to store all weapons
        const allWeapons = [];

        // Loop through each weapon ID and fetch the weapon details
        for (let i = 1; i <= numWeapons; i++) {
            const weaponId = i.toString();
            const weaponJSON = await contract.evaluateTransaction('ReadWeapon', weaponId);
            const weapon = JSON.parse(weaponJSON.toString());
            allWeapons.push(weapon);
        }

        res.status(200).json(allWeapons);
    } catch (error) {
        console.error(`Failed to fetch all weapons: ${error}`);
        res.status(500).send(`Failed to fetch all weapons: ${error}`);
    }
});


// Endpoint to update a weapon by ID
app.put('/weapons/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { name, description, userId, org } = req.body;
        const result = await submitTransaction(userId, org, 'UpdateWeapon', id, name, description);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to create a weapon purchase
app.post('/weapon-purchases', async (req, res) => {
    try {
        const { id, buyer, seller, weaponType, quantity, purchaseDate, deliveryDate, transactionStatus, userId} = req.body;
        const result = await submitTransaction(userId, 'CreateWeaponPurchase', id, buyer, seller, weaponType, quantity, purchaseDate, deliveryDate, transactionStatus);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to read a weapon purchase by ID
app.get('/weapon-purchases/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const userId = "Admin@bhupurchasingweapon.com";
        const result = await evaluateTransaction(userId, 'ReadWeaponPurchase', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});



app.get('/weapon-purchasesall/:userId', async (req, res) => {
    try {
        const { userId } = req.params;

        const contract = await getContract(userId);

        // Get the total number of weapon purchases
        const totalWeaponPurchases = await contract.evaluateTransaction('GetTotalWeapons');
        const numWeaponPurchases = parseInt(totalWeaponPurchases.toString());

        // Array to store all weapon purchases
        const allWeaponPurchases = [];

        // Loop through each weapon purchase ID and fetch the purchase details
        for (let i = 1; i <= numWeaponPurchases; i++) {
            const weaponPurchaseId = i.toString();
            const weaponPurchaseJSON = await contract.evaluateTransaction('ReadWeaponPurchase', weaponPurchaseId);
            const weaponPurchase = JSON.parse(weaponPurchaseJSON.toString());
            allWeaponPurchases.push(weaponPurchase);
        }

        res.status(200).json(allWeaponPurchases);
    } catch (error) {
        console.error(`Failed to fetch all weapon purchases: ${error}`);
        res.status(500).send(`Failed to fetch all weapon purchases: ${error}`);
    }
});

async function getContract(userId) {
    const walletPath = path.join(process.cwd(), 'wallet');
    // console.log("walletPath", walletPath)
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    // console.log("wallet", wallet)
    const identity = await wallet.get(userId);
    // console.log("identity", userId)
    if (!identity) {
        throw new Error(`An identity for the user "${userId}" does not exist in the wallet`);
    }

    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    console.log("connectionProfile", connectionProfile)

    const connectionOptions = { wallet, identity: identity, discovery: { enabled: false, asLocalhost: true } };
    console.log("connectionOptions", connectionOptions)
    
    await gateway.connect(connectionProfile, connectionOptions);

    const network = await gateway.getNetwork('purchaseweaponchannel');
    // console.log("network",network)
    const contract = network.getContract('purchasingweaponmgt');
    return contract;
}

async function submitTransaction(userId, functionName, ...args) {
    const contract = await getContract(userId);
    // console.log("contract", contract)
    // console.log("functionName", functionName)
    const result = await contract.submitTransaction(functionName, ...args);
    return result.toString();
}

async function evaluateTransaction(userId, functionName, ...args) {
    const contract = await getContract(userId);
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/index_html.js', function (req, res) {
    res.set('Content-Type', 'application/javascript');
    res.sendFile(__dirname + '/index_html.js');
});


module.exports = app
