$(document).ready(function () {
    // Function to fetch all weapons from the backend and populate the table
    function loadWeapons(userId) {
        $.ajax({
            url: `http://localhost:3000/weaponsall/${userId}`,
            type: 'GET',
            dataType: 'json',
            success: function (weapons) {
                $('#weaponList').empty(); // Clear the existing list
                weapons.forEach(function (weapon) {
                    if (weapon.name !== "") {
                        addWeaponToTable(weapon);
                    }
                });
            },
            error: function (error) {
                console.error(error);
                alert('Failed to load weapons. Please try again later.');
            }
        });
    }

    // Function to dynamically add weapon to the table
    function addWeaponToTable(weapon) {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>${weapon.id}</td>
            <td>${weapon.name}</td>
            <td>${weapon.description}</td>
            <td>${new Date(weapon.dateAdded).toLocaleString()}</td>
            <td>${weapon.addedByOrg}</td>
            <td><button class="btn btn-success buyButton">Buy</button></td>
        `;
        document.getElementById('weaponList').appendChild(row);
    }

    // Function to fetch all weapon purchases from the backend and populate the table
    function loadWeaponPurchases(userId) {
        $.ajax({
            url: `http://localhost:3000/weapon-purchasesall/${userId}`,
            type: 'GET',
            dataType: 'json',
            success: function (purchases) {
                $('#purchasedWeaponList').empty(); // Clear the existing list
                purchases.forEach(function (purchase) {
                    if (purchase.buyer !== "") {
                        addPurchasedWeaponToTable(purchase);
                    }
                });
            },
            error: function (error) {
                console.error(error);
                alert('Failed to load weapon purchases. Please try again later.');
            }
        });
    }

    // Function to dynamically add purchased weapon to the table
    function addPurchasedWeaponToTable(purchasedWeapon) {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>${purchasedWeapon.id}</td>
            <td>${purchasedWeapon.buyer}</td>
            <td>${purchasedWeapon.seller}</td>
            <td>${purchasedWeapon.weaponType}</td>
            <td>${purchasedWeapon.quantity}</td>
            <td>${new Date(purchasedWeapon.purchaseDate).toLocaleString()}</td>
            <td>${new Date(purchasedWeapon.deliveryDate).toLocaleString()}</td>
            <td>${purchasedWeapon.transactionStatus}</td>
        `;
        document.getElementById('purchasedWeaponList').appendChild(row);
    }

    // Function to handle weapon purchase
    function buyWeapon(buyerOrg, weaponId, weaponName, owner) {
        const purchaseDate = new Date().toISOString();
        const deliveryDate = new Date(Date.now() + 7 * 24 * 60 * 60 * 1000).toISOString(); // 1 week later
        const transactionStatus = "Completed";
        let userId = $('#buyerOrg').val();

        let buyererer;
        if (buyerOrg === "Admin@bhupurchasingweapon.com") {
            buyererer = "BhutanMSP";
        } else if (buyerOrg === "Admin@ruspurchasingweapon.com") {
            buyererer = "RussiaMSP";
        } else if (buyerOrg === "Admin@usapurchasingweapon.com") {
            buyererer = "UsaMSP";
        } 

        const purchasedWeapon = {
            id: weaponId,
            buyer: buyererer,
            seller: owner, // Replace with actual seller info
            weaponType: weaponName,
            quantity: 1,
            purchaseDate: purchaseDate,
            deliveryDate: deliveryDate,
            transactionStatus: transactionStatus,
            userId: userId // Replace with actual user ID
        };

        console.log("Purchased Weapon Details:", purchasedWeapon);

        $.ajax({
            url: 'http://localhost:3000/weapon-purchases',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(purchasedWeapon),
            success: function () {
                alert(`Weapon ID ${weaponId} bought by ${buyerOrg}`);
                addPurchasedWeaponToTable(purchasedWeapon);
            },
            error: function (error) {
                console.error(error);
                alert('Failed to create weapon purchase. Please try again later.');
            }
        });
    }

    // Add event listener to handle form submission
    $('#createWeaponForm').on('submit', function (event) {
        event.preventDefault();
        const name = $('#name').val();
        const description = $('#description').val();
        const userId = $('#org').val();

        const newWeapon = {
            name: name,
            description: description,
            userId: userId
        };

        $.ajax({
            url: 'http://localhost:3000/weapons',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(newWeapon),
            success: function () {
                alert('Weapon created successfully!');
                loadWeapons(userId);
            },
            error: function (error) {
                console.error(error);
                alert('Failed to create weapon. Please try again later.');
            }
        });
    });

    // Add event listener to handle buy button click
    $(document).on('click', '.buyButton', function () {
        const buyerOrg = $('#buyerOrg').val();
        if (!buyerOrg) {
            alert('Please select a buyer organization.');
            return;
        }
        const weaponRow = $(this).closest('tr');
        const weaponId = weaponRow.find('td:eq(0)').text();
        const weaponName = weaponRow.find('td:eq(1)').text();
        const owner = weaponRow.find('td:eq(4)').text();

        buyWeapon(buyerOrg, weaponId, weaponName, owner);
    });

    // Load weapons and weapon purchases when the window loads
    const userId = "Admin@bhupurchasingweapon.com";
    loadWeapons(userId);
    loadWeaponPurchases(userId);
});
