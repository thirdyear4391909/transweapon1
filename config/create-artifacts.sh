# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/transweapon1/bin" ] ; then
PATH="/workspaces/transweapon1/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm ./orderer/purchaseweapongenesis.block ./purchaseweaponchannel/purchaseweaponchannel.tx
# rm -rf ../../channel-artifacts/*

# Generate Crypto artifactes for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file 
export FABRIC_CFG_PATH=$PWD

# Generate the genesis block for the University Consortium Orderer
configtxgen -outputBlock ./orderer/purchaseweapongenesis.block -channelID ordererchannel -profile PurchaseWeaponOrdererGenesis


configtxgen -outputCreateChannelTx ./purchaseweaponchannel/purchaseweaponchannel.tx -channelID purchaseweaponchannel -profile PurchaseWeaponChannel

# peer channel create -c purchaseweaponchannel -f ./config/purchaseweaponchannel/purchaseweaponchannel.tx --outputBlock ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS


