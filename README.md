<!-- Generate the Orderer Genesis Block -->
configtxgen -outputBlock ./orderer/purchaseweapongenesis.block -channelID ordererchannel -profile PurchaseWeaponOrdererGenesis

<!-- Create the Channel Transaction -->
configtxgen -outputCreateChannelTx ./purchaseweaponchannel/purchaseweaponchannel.tx -channelID purchaseweaponchannel -profile PurchaseWeaponChannel


<!-- Set the Peer Environment -->
. tool-bins/set_peer_env.sh purchaseweapon


<!-- Create the Channel -->
peer channel create -c purchaseweaponchannel -f ./config/purchaseweaponchannel/purchaseweaponchannel.tx --outputBlock ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS


<!-- Create the Channel -->
peer channel create -c purchaseweaponchannel -f ./config/purchaseweaponchannel/purchaseweaponchannel.tx --outputBlock ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS


<!-- Join the Peer to the Channel -->
peer channel join -b ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS


<!-- Approve the Chaincode for the Organization -->
peer lifecycle chaincode approveformyorg -n weaponcertificatemgt -v 1.0 -C purchaseweaponchannel --sequence 1 --package-id $CC_PACKAGE_ID


<!-- Check Commit Readiness -->
peer lifecycle chaincode checkcommitreadiness -n weaponcertificatemgt -v 1.0 -C purchaseweaponchannel --sequence 1


<!-- Commit the Chaincode -->
peer lifecycle chaincode commit -n weaponcertificatemgt -v 1.0 -C purchaseweaponchannel --sequence 1

<!-- Query Committed Chaincodes -->
peer lifecycle chaincode querycommitted -n weaponcertificatemgt -C purchaseweaponchannel



<!-- Invoke the Chaincode -->
peer chaincode invoke -C purchaseweaponchannel -n weaponcertificatemgt -c '{"function":"IssueCertificate","Args":["1", "John Doe","PurchaseAuthorization","Weapon Store", "2024-01-01T00:00:00Z", "Store Manager","false","false"]}'


<!-- Query the Chaincode -->
peer chaincode query -C purchaseweaponchannel -n weaponcertificatemgt -c '{"function":"ReadCertificate","Args":["1"]}'


<!-- Invoke the Chaincode -->
peer chaincode invoke -C purchaseweaponchannel -n weaponcertificatemgt -c '{"function":"DeleteCertificate","Args":["1"]}'



