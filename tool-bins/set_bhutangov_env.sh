#!/bin/bash
export ORG_CONTEXT=bhupurchasingweapon
export ORG_NAME=Bhutan
export CORE_PEER_LOCALMSPID=BhutanMSP
# Logging specifications
export FABRIC_LOGGING_SPEC=INFO
# Location of the core.yaml
export FABRIC_CFG_PATH=/workspaces/transweapon1/config/bhutangov

# Address of the peer
export CORE_PEER_ADDRESS=bhutangov.bhupurchasingweapon.com:7051
# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/workspaces/transweapon1/config/crypto-config/peerOrganizations/bhupurchasingweapon.com/users/Admin@bhupurchasingweapon.com/msp

# Address of the orderer
export ORDERER_ADDRESS=orderer.bhupurchasingweapon.com:7050
export CORE_PEER_TLS_ENABLED=false
#### Chaincode related properties
export CC_NAME="purchasingweaponmgt"
export CC_PATH="./chaincodes/purchasingweaponmgt/"
export CC_CHANNEL_ID="purchaseweaponchannel"
export CC_LANGUAGE="golang"
# Properties of Chaincode
export INTERNAL_DEV_VERSION="1.0"
export CC_VERSION="1.0"
export CC2_PACKAGE_FOLDER="./chaincodes/packages/"
export CC2_SEQUENCE=1
export CC2_INIT_REQUIRED="--init-required"
# Create the package with this name
export
CC_PACKAGE_FILE="$CC2_PACKAGE_FOLDER$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION.tar.gz"
# Extracts the package ID for the installed chaincode
export CC_LABEL="$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION"
export GOFLAGS="-buildvcs=false"
peer channel list


# #!/bin/bash
# export ORG_CONTEXT=usapurchasingweapon
# export ORG_NAME=Usa
# export CORE_PEER_LOCALMSPID=UsaMSP
# # Logging specifications
# export FABRIC_LOGGING_SPEC=INFO
# # Location of the core.yaml
# export FABRIC_CFG_PATH=/workspaces/transweapon1/config/usagov

# # Address of the peer
# export CORE_PEER_ADDRESS=usagov.usapurchasingweapon.com:7053
# # Local MSP for the admin - Commands need to be executed as org admin
# export CORE_PEER_MSPCONFIGPATH=/workspaces/transweapon1/config/crypto-config/peerOrganizations/usapurchasingweapon.com/users/Admin@usapurchasingweapon.com/msp

# # Address of the orderer
# export ORDERER_ADDRESS=orderer.bhupurchasingweapon.com:7050
# export CORE_PEER_TLS_ENABLED=false
# #### Chaincode related properties
# export CC_NAME="purchasingweaponmgt"
# export CC_PATH="./chaincodes/purchasingweaponmgt/"
# export CC_CHANNEL_ID="purchaseweaponchannel"
# export CC_LANGUAGE="golang"
# # Properties of Chaincode
# export INTERNAL_DEV_VERSION="1.0"
# export CC_VERSION="1.0"
# export CC2_PACKAGE_FOLDER="./chaincodes/packages/"
# export CC2_SEQUENCE=1
# export CC2_INIT_REQUIRED="--init-required"
# # Create the package with this name
# export
# CC_PACKAGE_FILE="$CC2_PACKAGE_FOLDER$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION.tar.gz"
# # Extracts the package ID for the installed chaincode
# export CC_LABEL="$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION"
# export GOFLAGS="-buildvcs=false"
# peer channel list



# peer lifecycle chaincode install $CC_PACKAGE_FILE
# peer lifecycle chaincode approveformyorg -n purchasingweaponmgt -v 1.0 -C purchaseweaponchannel --sequence 1 --package-id $CC_PACKAGE_ID
#  peer lifecycle chaincode checkcommitreadiness -n purchasingweaponmgt -v 1.0 -C purchaseweaponchannel --sequence 1
# peer lifecycle chaincode commit -n purchasingweaponmgt -v 1.0 -C purchaseweaponchannel --sequence 1
# peer lifecycle chaincode querycommitted -n purchasingweaponmgt -C purchaseweaponchannel
# peer chaincode invoke -C purchaseweaponchannel -n purchasingweaponmgt -c '{"function":"CreateWeapon","Args":["Laser Gun", "High energy weapon"]}'
# peer chaincode invoke -C purchaseweaponchannel -n purchasingweaponmgt -c '{"function":"ReadWeapon","Args":["1"]}'
# peer chaincode invoke -C purchaseweaponchannel -n purchasingweaponmgt -c '{"function":"CreateWeaponPurchase","Args":["1", "buyerID", "sellerID", "Laser Gun", "10", "2024-05-19T00:00:00Z", "2024-06-19T00:00:00Z", "Pending"]}'
# vscode ➜ /workspaces/transweapon1 $ peer chaincode query -C purchaseweaponchannel -n purchasingweaponmgt -c '{"function":"ReadWeaponPurchase","Args":["1"]}'
# peer chaincode query -C purchaseweaponchannel -n purchasingweaponmgt -c '{"function":"GetTotalWeapons","Args":[]}'



