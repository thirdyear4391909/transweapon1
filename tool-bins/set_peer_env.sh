# #!/bin/bash

# # Sets the context for native peer commands

# function usage {
#     echo "Usage: . ./set_peer_env.sh ORG_NAME"
#     echo "Sets the organization context for native peer execution"
# }

# if [ "$1" == "" ]; then
#     usage
#     exit
# fi

# export ORG_CONTEXT=$1 MSP_ID="$(tr '[:lower:]' '[:upper:]' <<< ${ORG_CONTEXT:0:1})${ORG_CONTEXT:1}"
# export ORG_NAME=$MSP_ID

# Added this Oct 22
export CORE_PEER_LOCALMSPID=UsaMSP

# Logging specifications
export FABRIC_LOGGING_SPEC=INFO

# Location of the core.yaml
export FABRIC_CFG_PATH=/workspaces/transweapon1/config/russiagov


# Address of the peer
export CORE_PEER_ADDRESS=usagov.usapurchasingweapon.com:7053

# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/workspaces/transweapon1/config/crypto-config/peerOrganizations/usapurchasingweapon.com/users/Admin@usapurchasingweapon.com/msp

# Address of the orderer
export ORDERER_ADDRESS=orderer.bhupurchasingweapon.com:7050

export CORE_PEER_TLS_ENABLED=false



# bhupurchasingweapon.com
. tool-bins/set_peer_env.sh bhupurchasingweapon  bhutan
peer channel create -c purchaseweaponchannel -f ./config/purchaseweaponchannel/purchaseweaponchannel.tx --outputBlock ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS
peer channel join -b ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS

# ruspurchasingweapon.com
peer channel create -c purchaseweaponchannel -f ./config/purchaseweaponchannel/purchaseweaponchannel.tx --outputBlock ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS
peer channel join -b ./config/purchaseweaponchannel/purchaseweaponchannel.block -o $ORDERER_ADDRESS

# usapurchasingweapon.com
