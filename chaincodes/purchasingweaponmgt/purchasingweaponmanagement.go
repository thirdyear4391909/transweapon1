package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// Weapon represents a type of weapon added by an organization.
type Weapon struct {
	ID             string    `json:"id"`
	Name           string    `json:"name"`
	Description    string    `json:"description"`
	AddedByOrg     string    `json:"addedByOrg"`
	AddedByUser    string    `json:"addedByUser"`
	DateAdded      time.Time `json:"dateAdded"`
	LastUpdatedBy  string    `json:"lastUpdatedBy"`
	LastUpdatedOrg string    `json:"lastUpdatedOrg"`
	LastUpdated    time.Time `json:"lastUpdated"`
}
type WeaponPurchase struct {
	ID                string    `json:"id"`
	Buyer             string    `json:"buyer"`
	Seller            string    `json:"seller"`
	WeaponType        string    `json:"weaponType"`
	Quantity          int       `json:"quantity"`
	PurchaseDate      time.Time `json:"purchaseDate"`
	DeliveryDate      time.Time `json:"deliveryDate"`
	TransactionStatus string    `json:"transactionStatus"`
}

// WeaponContract provides functions for managing weapon types.
type WeaponContract struct {
	contractapi.Contract
}

// AutoIncrementIDKey is the key used to store the auto-incrementing ID counter in the ledger.
const AutoIncrementIDKey = "weaponIDCounter"

// CreateWeapon adds a new weapon to the ledger with an auto-incrementing ID.
func (c *WeaponContract) CreateWeapon(ctx contractapi.TransactionContextInterface, name string, description string) error {
	// Get the current counter value
	counter, err := c.getAndIncrementCounter(ctx)
	if err != nil {
		return err
	}

	// Create the weapon ID using the counter value
	id := strconv.Itoa(counter)

	clientIdentity := ctx.GetClientIdentity()
	mspID, err := clientIdentity.GetMSPID()
	if err != nil {
		return fmt.Errorf("error getting MSP ID: %v", err)
	}

	creator, err := clientIdentity.GetID()
	if err != nil {
		return fmt.Errorf("error getting creator ID: %v", err)
	}

	weapon := Weapon{
		ID:             id,
		Name:           name,
		Description:    description,
		AddedByOrg:     mspID,
		AddedByUser:    creator,
		DateAdded:      time.Now(),
		LastUpdatedBy:  creator,
		LastUpdatedOrg: mspID,
		LastUpdated:    time.Now(),
	}

	weaponJSON, err := json.Marshal(weapon)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, weaponJSON)
	if err != nil {
		return fmt.Errorf("failed to put weapon to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Created weapon: %s", id)
	err = ctx.GetStub().SetEvent("CreateWeapon", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return nil
}

// getAndIncrementCounter retrieves the current counter value and increments it.
func (c *WeaponContract) getAndIncrementCounter(ctx contractapi.TransactionContextInterface) (int, error) {
	counterAsBytes, err := ctx.GetStub().GetState(AutoIncrementIDKey)
	if err != nil {
		return 0, fmt.Errorf("failed to read counter from world state: %v", err)
	}

	var counter int
	if counterAsBytes == nil {
		counter = 0
	} else {
		counter, err = strconv.Atoi(string(counterAsBytes))
		if err != nil {
			return 0, fmt.Errorf("failed to convert counter to integer: %v", err)
		}
	}

	counter++
	counterAsBytes = []byte(strconv.Itoa(counter))

	err = ctx.GetStub().PutState(AutoIncrementIDKey, counterAsBytes)
	if err != nil {
		return 0, fmt.Errorf("failed to update counter in world state: %v", err)
	}

	return counter, nil
}

// GetTotalWeapons returns the total number of weapons.
func (c *WeaponContract) GetTotalWeapons(ctx contractapi.TransactionContextInterface) (int, error) {
	counterAsBytes, err := ctx.GetStub().GetState(AutoIncrementIDKey)
	if err != nil {
		return 0, fmt.Errorf("failed to read counter from world state: %v", err)
	}

	if counterAsBytes == nil {
		return 0, nil // No weapons added yet
	}

	counter, err := strconv.Atoi(string(counterAsBytes))
	if err != nil {
		return 0, fmt.Errorf("failed to convert counter to integer: %v", err)
	}

	return counter, nil
}

// ReadWeapon reads an existing weapon from the ledger.
func (c *WeaponContract) ReadWeapon(ctx contractapi.TransactionContextInterface, id string) (*Weapon, error) {
	weaponJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if weaponJSON == nil {
		return nil, fmt.Errorf("the weapon %s does not exist", id)
	}

	var weapon Weapon
	err = json.Unmarshal(weaponJSON, &weapon)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal weapon JSON: %v", err)
	}

	eventPayload := fmt.Sprintf("Read weapon: %s", id)
	err = ctx.GetStub().SetEvent("ReadWeapon", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register: %v", err)
	}

	return &weapon, nil
}

func (c *WeaponContract) UpdateWeapon(ctx contractapi.TransactionContextInterface, id string, name string, description string) error {
	weapon, err := c.ReadWeapon(ctx, id)
	if err != nil {
		return err
	}

	clientIdentity := ctx.GetClientIdentity()
	creator, err := clientIdentity.GetID()
	if err != nil {
		return fmt.Errorf("error getting creator ID: %v", err)
	}
	mspID, err := clientIdentity.GetMSPID()
	if err != nil {
		return fmt.Errorf("error getting MSP ID: %v", err)
	}

	weapon.Name = name
	weapon.Description = description
	weapon.LastUpdatedBy = creator
	weapon.LastUpdatedOrg = mspID
	weapon.LastUpdated = time.Now()

	weaponJSON, err := json.Marshal(weapon)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, weaponJSON)
	if err != nil {
		return fmt.Errorf("failed to put weapon to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Updated weapon: %s", id)
	err = ctx.GetStub().SetEvent("UpdateWeapon", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return nil
}

// CreateWeaponPurchase creates a new weapon purchase transaction.
func (c *WeaponContract) CreateWeaponPurchase(ctx contractapi.TransactionContextInterface, id string, buyer string, seller string, weaponType string, quantity int, purchaseDate time.Time, deliveryDate time.Time, transactionStatus string) error {
	weaponPurchase := WeaponPurchase{
		ID:                id,
		Buyer:             buyer,
		Seller:            seller,
		WeaponType:        weaponType,
		Quantity:          quantity,
		PurchaseDate:      purchaseDate,
		DeliveryDate:      deliveryDate,
		TransactionStatus: transactionStatus,
	}

	weaponPurchaseJSON, err := json.Marshal(weaponPurchase)
	if err != nil {
		return fmt.Errorf("failed to marshal weapon purchase data: %v", err)
	}

	err = ctx.GetStub().PutState(id, weaponPurchaseJSON)
	if err != nil {
		return fmt.Errorf("failed to put weapon purchase to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Created weapon purchase: %s", id)
	err = ctx.GetStub().SetEvent("CreateWeaponPurchase", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("failed to register event for creating weapon purchase: %v", err)
	}

	return nil
}

// ReadWeaponPurchase reads an existing weapon purchase transaction.
func (c *WeaponContract) ReadWeaponPurchase(ctx contractapi.TransactionContextInterface, id string) (*WeaponPurchase, error) {
	weaponPurchaseJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if weaponPurchaseJSON == nil {
		return nil, fmt.Errorf("the weapon purchase %s does not exist", id)
	}

	var weaponPurchase WeaponPurchase
	err = json.Unmarshal(weaponPurchaseJSON, &weaponPurchase)
	if err != nil {
		return nil, err
	}

	eventPayload := fmt.Sprintf("Read weapon purchase: %s", id)
	err = ctx.GetStub().SetEvent("ReadWeaponPurchase", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register. %v", err)
	}

	return &weaponPurchase, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(WeaponContract))
	if err != nil {
		fmt.Printf("Error create weapon contract: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting weapon contract: %s", err.Error())
	}
}
